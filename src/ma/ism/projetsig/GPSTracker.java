package ma.ism.projetsig;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class GPSTracker extends Service implements LocationListener {

	private static final String SERVER_URL = "http://192.168.56.1:8080/webservice/rest/";
	static boolean isStarting = false;
	 boolean isGPSEnabled = false;
	 JSONObject jsonData;

	//	private  Context mContext;
	 Intent intentService;
	Location location; // location
	double latitude; // latitude
	double longitude; // longitude

	// The minimum distance to change Updates in meters
	private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 100; // 10 meters

	// The minimum time between updates in milliseconds
	private static final long MIN_TIME_BW_UPDATES = 10000; // 10 sec

	// Declaring a Location Manager
	protected LocationManager locationManager;

	public GPSTracker() {
	}

	public GPSTracker(Context context) {
		//		this.mContext = context;
		getLocation();
	}

	public Location getLocation() {
		try {
			locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);

			// getting GPS status
			isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
			if (isGPSEnabled) {
				Log.d("GPS Enabled", "GPS Enabled");
				locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
						MIN_TIME_BW_UPDATES,
						MIN_DISTANCE_CHANGE_FOR_UPDATES,
						this);
				if (locationManager != null){
					location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
					if (location != null) {
						latitude = location.getLatitude();
						longitude = location.getLongitude();
					}
				}
			}else {
				Log.d("GPS is not Enabled", "GPS is not Enabled");
			}


		} catch (Exception e) {
			e.printStackTrace();
		}

		return location;
	}


	/**
	 * Function to get latitude
	 * */
	public double getLatitude() {
		if (location != null) {
			latitude = location.getLatitude();
		}
		return latitude;
	}

	/**
	 * Function to get longitude
	 * */
	public double getLongitude() {
		if (location != null) {
			longitude = location.getLongitude();
		}
		return longitude;
	}

	
	public void stopUsingGPS(){
		if(locationManager != null){
			locationManager.removeUpdates(GPSTracker.this);
		}		
	}

	int i =9;
	@Override
	public void onLocationChanged(Location location) {
			if(i==9){
				getLocation();
				//			getLongitude();
				//			getLatitude();
				//Log.i("speed: ", ""+speed);
				Log.i("latitude: ", ""+latitude);
				Log.i("longitude: ", ""+longitude);
				new SendData().execute();
				MapActivity.setJsonData(jsonData);
				i=0;
			}else {
				getLocation();
				Log.d("i: ", ""+i);

				i++;}		
		
	}

	@Override
	public void onProviderDisabled(String provider) {
	}

	@Override
	public void onProviderEnabled(String provider) {
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO: Return the communication channel to the service.
		throw new UnsupportedOperationException("Not yet implemented");
	}

	@Override
	public void onCreate() {
		Toast.makeText(this, "The new Service was Created", Toast.LENGTH_SHORT).show();
		isStarting = true;
	}

	@Override
	public void onStart(Intent intent, int startId) {
		getLocation();
		intentService = intent;
		new SendData().execute();
		// For time consuming an long tasks you can launch a new thread here...
		Toast.makeText(this, " Service Started", Toast.LENGTH_SHORT).show();

	}

	@Override
	public void onDestroy() {
		stopUsingGPS();
		Toast.makeText(this, "Service Stoped", Toast.LENGTH_SHORT).show();
		isStarting = false;

	}
	
	class SendData extends AsyncTask<String, String, String> {
		@Override
		protected String doInBackground(String... arg0) {
			intentService.getStringExtra("type");
			try { 
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair("latitude",latitude +""));
				params.add(new BasicNameValuePair("longitude", longitude+""));
				params.add(new BasicNameValuePair("matricule", intentService.getStringExtra("matricule")));
				params.add(new BasicNameValuePair("type",intentService.getStringExtra("type")));
				JSONParser jp = new JSONParser();
				jsonData = jp.makeHttpRequest(SERVER_URL,"POST", params);
				System.out.println(jsonData.get("noAnomaly"));
				JSONArray anomalyList = jsonData.getJSONArray("anomalyList");
				MapActivity.setJsonData(jsonData);
				Intent intent = new Intent();
				//Binder b = new Binder();
			
				intent.putExtra("json", jsonData.toString());
				System.out.println(jsonData.toString());
				for(int i = 0; i < anomalyList.length(); i++){
					JSONObject anomaly = anomalyList.getJSONObject(i);

					System.out.println(anomaly.getString("longitude"));
					System.out.println(anomaly.getString("latitude"));
					System.out.println(anomaly.getString("anomalyType"));
					System.out.println(anomaly.getString("matricule"));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
		
		}
	}

}
