package ma.ism.projetsig;

import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback {
	Button btnStopTracking;
	Fragment frgm;
	MapFragment mapFragment;
	GPSTracker gps;
	static JSONObject jsonData = null;
	GoogleMap map;
	public static void setJsonData(JSONObject json) {
		jsonData = json;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		btnStopTracking = (Button) findViewById(R.id.btnStopTrackingMe);
		btnStopTracking.setBackgroundColor(Color.RED);
		mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.mapFragment);
		mapFragment.getMapAsync(this);
		map = mapFragment.getMap();
		map.setMyLocationEnabled(true);
		UiSettings uiSettings = map.getUiSettings();
		uiSettings.setCompassEnabled(true);
		uiSettings.setZoomControlsEnabled(true);
		uiSettings.setMyLocationButtonEnabled(true);
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(33.9804,-6.86658), 15));
		map.setTrafficEnabled(true);
		map.setOnMapClickListener(new OnMapClickListener() {
		
			@Override
			public void onMapClick(LatLng arg0) {
				updateMap();

			}
		});
		btnStopTracking.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				stopService();
			}
		});
	}

	// Method to start the service
	public void startService() {
		Intent intent = new Intent(this, GPSTracker.class);
		startService(intent);
		Log.i("startService()", "startService()");
	}

	// Method to stop the service
	public void stopService() {
		Intent intent = new Intent(this, GPSTracker.class);
		stopService(intent);
		finish();
	}

	public void updateMap() {
		Log.d("/////", "////");
		Log.d("/////", "////");
		Log.d("/////", "////");
		Log.d("/////", "////");
		Log.d("/////", "////");
		Log.d("/////", "////");

		try {
			map.clear();
			if (jsonData != null)
				if (jsonData.getInt("noAnomaly") == 0) {
					JSONArray anomalyList = jsonData.getJSONArray("anomalyList");
					for (int i = 0; i < anomalyList.length(); i++) {
						JSONObject anomaly = anomalyList.getJSONObject(i);
						Double longitude = anomaly.getDouble("longitude");
						Double latitude = anomaly.getDouble("latitude");
						if ((anomaly.getString("anomalyType")).equals("accident")) {
							map.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude))
									.title("accident mat:" + anomaly.getString("matricule"))
									.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
						}
						if ((anomaly.getString("anomalyType")).equals("excesVitesse")) {
							map.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude))
									.title("excesVitesse mat:" + anomaly.getString("matricule"))
									.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
						}
					}
				}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onMapReady(GoogleMap map) {

		MapActivity.this.runOnUiThread(new Runnable() {
			public void run() {

				final Handler handler = new Handler();
				Timer timer = new Timer();
				TimerTask task = new TimerTask() {
					@Override
					public void run() {
						handler.post(new Runnable() {
							public void run() {
								Log.d("UI thread", "I am the UI thread");
								updateMap();
							}
						});
					}
				};
				timer.schedule(task, 0, 10000); // it executes this every 1000ms
			}
		});
		/////

	}

}
