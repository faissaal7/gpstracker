package ma.ism.projetsig;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends Activity {
	Button btnStartTracking;
	RadioGroup rg ;
	EditText edtxt;
	// GPSTracker class
	GPSTracker gps;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		edtxt = (EditText)findViewById(R.id.EdtxtMatriculate);
		btnStartTracking = (Button) findViewById(R.id.btnStartTrackingMe);
		rg = (RadioGroup) findViewById(R.id.policeORambulance);
		btnStartTracking.setBackgroundColor(Color.GREEN);
		///////////////////////
		btnStartTracking.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				startService();
				}			
		});
		if(GPSTracker.isStarting){
			Intent mapIntent = new Intent(this, MapActivity.class);
			startActivity(mapIntent);
			Log.i("startMapActivity", "startMapActivity");
			finish();
		}
	}

	// Method to start the service



	public void startService() {
		LocationManager locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
		boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		
		
		if(isGPSEnabled){
			if(isValid()){
			//start service
			Intent intent = new Intent(this, GPSTracker.class);			
			String radiovalue = ((RadioButton)findViewById(rg.getCheckedRadioButtonId())).getText().toString();  
			intent.putExtra("type", radiovalue);
			Log.d("radio", radiovalue);
			intent.putExtra("matricule", edtxt.getText().toString());
			Log.d("matricule", edtxt.getText().toString());
			startService(intent);
			Log.i("startService", "startService");
			//start MAP activity 
			Intent mapIntent = new Intent(this, MapActivity.class);
			startActivity(mapIntent);
			Log.i("startMapActivity", "startMapActivity");
			finish();
			}else {
				Toast.makeText(this, "please, make sure all input data are correct",Toast.LENGTH_SHORT).show();
			}
		}else{
			Toast.makeText(this, "please, enable GPS",Toast.LENGTH_SHORT).show();
			showSettingsAlert();
		}
	}
	public boolean isValid(){
		//if(rg.getCheckedRadioButtonId()!=0 && rg.getCheckedRadioButtonId()!=1)return false;
		if(edtxt.getText().toString().isEmpty())return false;
		return true;
	}
	public void showSettingsAlert() {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

		// Setting Dialog Title
		alertDialog.setTitle("GPS is settings");

		// Setting Dialog Message
		alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

		// On pressing Settings button
		alertDialog.setPositiveButton("Settings",
				new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				Intent intent = new Intent(
						Settings.ACTION_LOCATION_SOURCE_SETTINGS);
				startActivity(intent);
			}
		});

		// on pressing cancel button
		alertDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});

		// Showing Alert Message
		alertDialog.show();
	}
}
